Format: 1.0
Source: linux-5.4.2-kali
Binary: linux-image-5.4.2-kali, linux-headers-5.4.2-kali, linux-libc-dev, linux-image-5.4.2-kali-dbg
Architecture: amd64
Version: 5.4.2-1
Maintainer: root <root@blade>
Homepage: http://www.kernel.org/
Build-Depends: bc, kmod, cpio, bison, flex | flex:native, libelf-dev, libssl-dev:native
Package-List:
 linux-headers-5.4.2-kali deb kernel optional arch=amd64
 linux-image-5.4.2-kali deb kernel optional arch=amd64
 linux-image-5.4.2-kali-dbg deb debug optional arch=amd64
 linux-libc-dev deb devel optional arch=amd64
Checksums-Sha1:
 d7869a6ec5e040f3b84896c2a1ca3bda032ff8e2 177079437 linux-5.4.2-kali_5.4.2.orig.tar.gz
 43a3b2aaea66f2ab5e58992e95ca18e12a9ba02c 238734 linux-5.4.2-kali_5.4.2-1.diff.gz
Checksums-Sha256:
 cd7309131dd643c9ac841cefcae97614a89aee1a1605aa95c377e3cd8a0e3c76 177079437 linux-5.4.2-kali_5.4.2.orig.tar.gz
 18fcf4daa11213b0680b8e182af1accc99a6fe6cd62a3e63f8102889dd18fbe1 238734 linux-5.4.2-kali_5.4.2-1.diff.gz
Files:
 158096881762711c30ae3ef8c01d2d1c 177079437 linux-5.4.2-kali_5.4.2.orig.tar.gz
 73e43062c32591ffdf05427c32f0ee11 238734 linux-5.4.2-kali_5.4.2-1.diff.gz
