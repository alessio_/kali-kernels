Format: 1.0
Source: linux-5.4.3-kali
Binary: linux-image-5.4.3-kali, linux-headers-5.4.3-kali, linux-libc-dev, linux-image-5.4.3-kali-dbg
Architecture: amd64
Version: 5.4.3-kali-1
Maintainer: root <root@blade>
Homepage: http://www.kernel.org/
Build-Depends: bc, kmod, cpio, bison, flex | flex:native, libelf-dev, libssl-dev:native
Package-List:
 linux-headers-5.4.3-kali deb kernel optional arch=amd64
 linux-image-5.4.3-kali deb kernel optional arch=amd64
 linux-image-5.4.3-kali-dbg deb debug optional arch=amd64
 linux-libc-dev deb devel optional arch=amd64
Checksums-Sha1:
 caa14a53c0898b22289008775ec581bc800ff098 176646158 linux-5.4.3-kali_5.4.3-kali.orig.tar.gz
 67e8eb247fcca64f907fca5ee3100f6d29e0902a 239035 linux-5.4.3-kali_5.4.3-kali-1.diff.gz
Checksums-Sha256:
 ed52d117bd2ee0fb9f31db3aca66f51ebed5ec8d680f64a433caeaddc307665f 176646158 linux-5.4.3-kali_5.4.3-kali.orig.tar.gz
 c02171e94dc72d4e07ace49a25e99f4c52c544f16443f509ca4391e067f4dd2c 239035 linux-5.4.3-kali_5.4.3-kali-1.diff.gz
Files:
 60f442b922be972ab602d9dac592301a 176646158 linux-5.4.3-kali_5.4.3-kali.orig.tar.gz
 19c5107aa3338245ef5b5f617d8381c1 239035 linux-5.4.3-kali_5.4.3-kali-1.diff.gz
